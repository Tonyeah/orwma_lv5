package com.example.rma_lv5;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private static final int RB1_ID = 1;//first radio button id
    private static final int RB2_ID = 2;//second radio button id
    private static final int RB3_ID = 3;//third radio button id


    ImageView ivPerson1;
    ImageView ivPerson2;
    ImageView ivPerson3;
    Button btnInspiration;
    Button btnTop3;
    Button btnEditDescription;
    RadioButton rbtnPerson1;
    RadioButton rbtnPerson2;
    RadioButton rbtnPerson3;
    RadioGroup rgPersonSelection;
    TextView tvAboutPerson1;
    TextView tvAboutPerson2;
    TextView tvAboutPerson3;
    EditText etDescriptionChange;

    private List<String> inspirations;
    private Random random = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeUI();
        initializeAboutPersons();
        initializeInspirations();
    }

    private void initializeInspirations() {
        inspirations = new ArrayList<>();
        inspirations.add("\"The age of Men is over. The time of the Orc has come!\" - Gothmog");
        inspirations.add("\"Rock in the pool\n" +
                "So nice and cool\n" +
                "So juicy sweet!\n" +
                "Now we wish\n" +
                "To catch a fish\n" +
                "So juicy sweet!\"\n - Smeagol");
        inspirations.add("Gothmog always wanted to be a professional makeup artist.");
        inspirations.add("Gollum hates Lembas bread. (Elvish bread)");
        inspirations.add("Gothmog liked to launch various things with his catapult");
        inspirations.add("The name Gollum was derived from the sound of his disgusting gurgling, choking cough");
        inspirations.add("\"Smeagol wouldn't hurt a fly!\" - Smeagol");
    }

    private void initializeAboutPersons() {
        tvAboutPerson1 = (TextView) findViewById(R.id.tvPerson1About);
        tvAboutPerson2 = (TextView) findViewById(R.id.tvPerson2About);
        tvAboutPerson3 = (TextView) findViewById(R.id.tvPerson3About);
        tvAboutPerson1.append("Smeagol was a tour guide of Middle-earth and an excellent pathfinder. He also liked fish and he liked them raaaw.");
        tvAboutPerson2.append("Gollum was a jewlery expert. He spent his life searching for precioussss artifacts. ");
        tvAboutPerson3.append("Gothmog was the lieutenant of the Witch-king in the Third Age from Minas Morgul, notably serving at the Battle of the Pelennor Fields.");

    }

    private void initializeUI() {
        this.ivPerson1 = (ImageView) findViewById( R.id.ivPerson1);
        this.ivPerson2 = (ImageView) findViewById( R.id.ivPerson2);
        this.ivPerson3 = (ImageView) findViewById( R.id.ivPerson3);
        this.btnInspiration = (Button) findViewById(R.id.btnInspiration);
        this.btnTop3 = (Button) findViewById(R.id.btnTop3);
        this.btnEditDescription = (Button) findViewById(R.id.btnEditDescription);
        this.rgPersonSelection = (RadioGroup) findViewById(R.id.rgPersonSelection);
        this.rbtnPerson1 = (RadioButton) findViewById(R.id.rbtnPerson1);
        this.rbtnPerson2 = (RadioButton) findViewById(R.id.rbtnPerson2);
        this.rbtnPerson3 = (RadioButton) findViewById(R.id.rbtnPerson3);
        rbtnPerson1.setId(RB1_ID);
        rbtnPerson2.setId(RB2_ID);
        rbtnPerson3.setId(RB3_ID);
        this.etDescriptionChange = (EditText) findViewById(R.id.etEditDescription);
        this.btnEditDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPersonDescription(etDescriptionChange.getText().toString(), rgPersonSelection.getCheckedRadioButtonId());
            }
        });
        this.btnInspiration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                displayToast(inspirations.get(random.nextInt(inspirations.size())));
            }
        });
        this.ivPerson1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                ivPerson1.setVisibility(View.INVISIBLE); //ili View.GONE
            }
        });
        this.ivPerson2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                ivPerson2.setVisibility(View.INVISIBLE); //ili View.GONE
            }
        });
        this.ivPerson3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                ivPerson3.setVisibility(View.INVISIBLE); //ili View.GONE
            }
        });
    }

    private void displayToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    private void setPersonDescription(String text, Integer PersonNumber){

        switch (PersonNumber) {
            case 1:
                tvAboutPerson1.setText(text);
                etDescriptionChange.getText().clear();
                break;
            case 2:
                tvAboutPerson2.setText(text);
                etDescriptionChange.getText().clear();
                break;
            case 3:
                tvAboutPerson3.setText(text);
                etDescriptionChange.getText().clear();
                break;
        }
    }
}
